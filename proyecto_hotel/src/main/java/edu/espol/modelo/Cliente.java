/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.espol.modelo;

import java.util.ArrayList;

/**
 *
 * @author Jandry J. Moreira Q
 */
public class Cliente {
    private String nombre;
    private String cedula;
    private ArrayList<Reserva>reservacion;
    
    public Cliente(String nombre, String cedula ){
        this.nombre=nombre;
        reservacion=new ArrayList<>();
        this.cedula=cedula;
    }
}
