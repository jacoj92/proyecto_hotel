/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.espol.proyecto_hotel;


import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Jandry J. Moreira Q
 */
public class InicioController implements Initializable {

    @FXML
    private ComboBox<String> cboConsultaCreacion;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //cboConsultaCreacion.getItems().addAll(App.opcionesConsulta);
        cboConsultaCreacion.getItems().addAll("Creacion de hoteles y habitaciones", "Consulta de hoteles y habitaciones");
        
    }    
    
    @FXML
    public void seleccionConsulta(){
        if (cboConsultaCreacion.getValue().toString().equals("Creacion de hoteles y habitaciones")){
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/edu/espol/proyecto_hotel/creacion_hoteles_habitaciones.fxml"));
                Parent root = loader.load();
                creacion_hoteles_habitacionesController controlador=loader.getController();

                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);  //no deja volver a la ventana anterior 
                stage.setScene(scene);
                stage.showAndWait();  
            
            } catch (IOException ex) {
                System.out.println("ERROR DESCONOCIDO: "+ ex);
            }
        }
    }
    
}
